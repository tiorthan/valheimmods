﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using BepInEx;
using BepInEx.Configuration;
using CarTioGrapher.Extensions;
using On.Steamworks;
using SimpleJson;
using UnityEngine;
using Logger = Jotunn.Logger;

namespace CarTioGrapher
{
    public class Configuration
    {
        public class PinConfiguration
        {
            private const string PatternKey = "pattern";
            private const string AutoPinKey = "autoPin";
            private const string PinNameKey = "pinName";
            private const string PinTypeKey = "pinType";

            public Regex Pattern { get; }
            public bool AutoPin { get; }
            public string PinName { get; }
            public Minimap.PinType PinType { get; }

            public PinConfiguration(JsonObject jsonObject)
            {
                Pattern = new Regex(jsonObject.ValueOrDefault(PatternKey, string.Empty));
                AutoPin = jsonObject.BoolValue(AutoPinKey);
                PinName = jsonObject.ValueOrDefault(PinNameKey, string.Empty);
                PinType = jsonObject.EnumValue(PinTypeKey, Minimap.PinType.Icon3);
            }

            public PinConfiguration(string pattern, bool autoPin, string pinName, Minimap.PinType pinType)
            {
                Pattern = new Regex(pattern);
                AutoPin = autoPin;
                PinName = pinName;
                PinType = pinType;
            }

            public override string ToString()
            {
                return $"PinConfiguration(pattern={Pattern}, autoPin={AutoPin}, pinName={PinName}, pinType={PinType}";
            }
        }

        private static IEnumerable<PinConfiguration> DefaultPinconfigurations => new[]
        {
            new PinConfiguration("HoverText: \\$piece_deposit_copper", true, "$piece_deposit_copper",
                Minimap.PinType.Icon3),
            new PinConfiguration("HoverText: \\$piece_deposit_tin", true, "$piece_deposit_tin",
            Minimap.PinType.Icon3)
        };

        public static ConfigEntry<KeyboardShortcut> AddPinKeyEntry;
        public static ConfigEntry<bool> DebugHudEnabled;
        public static ConfigEntry<bool> DebugLogEnabled;

        public static readonly List<PinConfiguration> PinConfigurations = new List<PinConfiguration>();

        public static void Init(ConfigFile configFile)
        {
            AddPinKeyEntry = configFile.Bind("Key Bindings", "addPinKey", new KeyboardShortcut(KeyCode.P),
                "Key to add a pin to the map for the object the crosshairs is pointing at.");
            DebugHudEnabled = configFile.Bind("Debug Outputs", "hudDebugOutput", false,
                "Show debug information about the currently targeted object in the HUD.");
            DebugLogEnabled =
                configFile.Bind("Debug Outputs", "logDebugOutput", true,
                    "Show debug information about the currently targeted object in the log.");

            LoadPinConfigurations();
        }

        private static void LoadPinConfigurations()
        {
            var fileName = Path.Combine(Paths.ConfigPath, "cartiographer.pins.json");
            if (!File.Exists(fileName))
            {
                PinConfigurations.AddRange(DefaultPinconfigurations);
            }
            else
            {
                var configFileContents = File.ReadAllText(fileName);
                var deserializedConfig = SimpleJson.SimpleJson.DeserializeObject(configFileContents);
                if (deserializedConfig is JsonArray configItemArray)
                {
                    PinConfigurations.AddRange(configItemArray.OfType<JsonObject>()
                        .Select(item => new PinConfiguration(item)));
                }
            }

            Logger.LogDebug($"Loaded pin configurations\n{string.Join("\n", PinConfigurations)}");
        }
    }
}