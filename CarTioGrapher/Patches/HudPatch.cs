﻿using CarTioGrapher.Pins;
using Jotunn.Utils;
using UnityEngine;

namespace CarTioGrapher.Patches
{
    public static class HudPatch
    {
        private static GameObject _lastHover;

        [PatchInit(1)]
        public static void Init()
        {
            On.Hud.UpdateCrosshair += GenerateHoverPinCandidates;
        }

        private static void GenerateHoverPinCandidates(On.Hud.orig_UpdateCrosshair orig, Hud self, Player player,
            float bowDrawPercentage)
        {
            orig(self, player, bowDrawPercentage);
            var hoverObject = player.GetHoverObject();
            if (hoverObject != _lastHover)
            {
                _lastHover = hoverObject;
                MapPins.Instance.OnHoverUpdate(hoverObject);
            }
        }
    }
}