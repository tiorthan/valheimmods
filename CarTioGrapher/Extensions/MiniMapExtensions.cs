﻿using CarTioGrapher.Pins;

namespace CarTioGrapher.Extensions
{
    public static class MiniMapExtensions
    {
        public static Minimap.PinData AddPin(this Minimap minimap, PinCandidate pinCandidate) =>
            minimap.AddPin(pinCandidate.Position, pinCandidate.Type, pinCandidate.Name, true, false);
    }
}