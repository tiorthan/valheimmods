﻿using System;
using SimpleJson;

namespace CarTioGrapher.Extensions
{
    public static class JsonObjectExtensions
    {
        public static T ValueOrDefault<T>(this JsonObject jsonObject, string key, T defaultValue) where T : class
        {
            object result;
            jsonObject.TryGetValue(key, out result);
            return result as T ?? defaultValue;
        }

        public static T EnumValue<T>(this JsonObject jsonObject, string key, T defaultValue) where T : struct, Enum
        {
            var stringValue = jsonObject.ValueOrDefault<string>(key, null);
            return Enum.TryParse(stringValue, out T result) ? result : defaultValue;
        }

        public static bool BoolValue(this JsonObject jsonObject, string key)
        {
            return jsonObject.ContainsKey(key) && jsonObject[key] is bool && (bool) jsonObject[key];
        }
    }
}