﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Logger = Jotunn.Logger;

namespace CarTioGrapher.Pins
{
    public class GameObjectDescriptor
    {
        public GameObject GameObject { get; }
        public ISet<string> ComponentInfo { get; } = new SortedSet<string>();

        public GameObjectDescriptor(GameObject hoverObject)
        {
            GameObject = hoverObject;
            LoadHoverText();
        }

        private void LoadHoverText()
        {
            var hoverText = GameObject.GetComponentInParent<HoverText>();
            if (hoverText)
            {
                ComponentInfo.Add($"HoverText: {hoverText.m_text}");
            }
        }

        public override string ToString()
        {
            var components = string.Join(", ",
                GameObject.GetComponentsInParent<object>().Select(x => x.GetType().ToString()));
            return $"GameObject at {GameObject.transform.position}\n{components}\n{Description("\n")}";
        }

        private string Description(string separator = ", ") => string.Join(separator, ComponentInfo);

        public bool Matches(Regex regex)
        {
            var isMatch = regex.IsMatch(Description());
            Logger.LogDebug($"Description: {Description()}, Regex: {regex}, match: {isMatch}");
            return isMatch;
        }
    }
}