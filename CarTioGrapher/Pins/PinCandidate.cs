﻿using System.Linq;
using UnityEngine;

namespace CarTioGrapher.Pins
{
    public class PinCandidate
    {
        public Vector3 Position { get; }
        public Minimap.PinType Type { get; }
        public string Name { get; }
        public bool Auto { get; }

        private PinCandidate(GameObjectDescriptor descriptor, Configuration.PinConfiguration configuration)
        {
            Position = descriptor.GameObject.transform.position;
            Type = configuration.PinType;
            Name = configuration.PinName;
            Auto = configuration.AutoPin;
        }

        public static PinCandidate FromDescriptor(GameObjectDescriptor descriptor)
        {
            var firstMatch =
                Configuration.PinConfigurations.FirstOrDefault(config => descriptor.Matches(config.Pattern));

            return (firstMatch != null) ? new PinCandidate(descriptor, firstMatch) : null;
        }
    }
}