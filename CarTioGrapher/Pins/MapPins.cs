﻿using CarTioGrapher.Extensions;
using HarmonyLib;
using Jotunn.Managers;
using UnityEngine;
using Logger = Jotunn.Logger;

namespace CarTioGrapher.Pins
{
    public class MapPins : MonoBehaviour
    {
        public static MapPins Instance { get; private set; }

        private GameObject _debugTextElement;
        private PinCandidate _currentPinCandidate;

        private void Awake()
        {
            Instance = this;
        }

        private void ClearDebugText()
        {
            if (_debugTextElement != null)
            {
                Destroy(_debugTextElement);
            }
        }

        public void OnHoverUpdate(GameObject hoverObject)
        {
            ClearDebugText();
            _currentPinCandidate = null;

            if (hoverObject == null) return;


            var descriptor = new GameObjectDescriptor(hoverObject);
            ShowDebugText(descriptor);
            HandleNewPinCandidate(PinCandidate.FromDescriptor(descriptor));
        }

        private void HandleNewPinCandidate(PinCandidate candidate)
        {
            if (candidate == null) return;
            if (candidate.Auto)
            {
                TryAddPin(candidate);
                return;
            }

            _currentPinCandidate = candidate;
        }

        private void TryAddPin(PinCandidate candidate)
        {
            var map = Minimap.instance;

            if (!Traverse.Create(map).Method("HaveSimilarPin", new object[]
            {
                candidate.Position, candidate.Type, candidate.Name, true
            }).GetValue<bool>())
            {
                map.AddPin(candidate);
            }
        }

        private void ShowDebugText(GameObjectDescriptor descriptor)
        {
            if (Configuration.DebugLogEnabled.Value)
            {
                Logger.LogDebug(descriptor.ToString());
            }

            if (Configuration.DebugHudEnabled.Value)
            {
                _debugTextElement = GUIManager.Instance.CreateText(descriptor.ToString(),
                    GUIManager.PixelFix.transform, new Vector2(-5f, -1f), new Vector2(-5f, -1f), Vector2.zero,
                    GUIManager.Instance.AveriaSerif, 18, GUIManager.Instance.ValheimOrange, true, Color.black, 500f,
                    300f, false);
            }
        }

        private void Update()
        {
            if (Configuration.AddPinKeyEntry.Value.IsDown() && _currentPinCandidate != null)
            {
                Logger.LogDebug("Pin key pressed and candidate set.");
                TryAddPin(_currentPinCandidate);
                _currentPinCandidate = null;
            }
        }
    }
}