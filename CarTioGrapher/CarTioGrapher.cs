﻿using BepInEx;
using CarTioGrapher.Pins;
using Jotunn;

namespace CarTioGrapher
{
    [BepInPlugin("de.tiorthan.valheim.cartiographer", "CarTioGrapher", "0.1.0")]
    [BepInDependency(Main.ModGuid)]
    public class CarTioGrapher : BaseUnityPlugin
    {
        private void Awake()
        {
            Configuration.Init(Config);
            gameObject.AddComponent<MapPins>();
        }
    }
}